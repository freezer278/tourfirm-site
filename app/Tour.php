<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    const SORT_PRICE_ASC = 'price-asc';
    const SORT_PRICE_DESC = 'price-desc';
    const SORT_STARS_ASC = 'stars-asc';
    const SORT_STARS_DESC = 'stars-desc';

    protected $fillable = [
        'hotel_name',
        'dest_city',
        'dest_country',
        'price',
        'stars',
        'days_in_tour',
    ];

    protected $perPage = 20;

    public function getDestinationAttribute()
    {
        return $this->dest_country . ', ' . $this->dest_city;
    }

    public function images()
    {
        return $this->hasMany(TourImage::class, 'tour_id', 'id');
    }

    public function mainImage()
    {
        return $this->images()->where('main_image', 1)->first();
    }
}
