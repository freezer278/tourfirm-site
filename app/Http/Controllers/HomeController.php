<?php

namespace App\Http\Controllers;

use App\Review;
use App\Tour;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected function getToursWithSort(string $sort)
    {
        $orderByField = 'id';
        $orderByValue = 'desc';

        switch ($sort) {
            case Tour::SORT_PRICE_ASC :
                $orderByField = 'price';
                $orderByValue = 'asc';
                break;
            case Tour::SORT_PRICE_DESC :
                $orderByField = 'price';
                $orderByValue = 'desc';
                break;
            case Tour::SORT_STARS_ASC :
                $orderByField = 'stars';
                $orderByValue = 'asc';
                break;
            case Tour::SORT_STARS_DESC :
                $orderByField = 'stars';
                $orderByValue = 'desc';
                break;
        }

        return Tour::orderBy($orderByField, $orderByValue)->paginate();
    }

    /**
     * Show the application dashboard.
     *
     */
    public function index()
    {
        $hotTours = Tour::orderBy('id', 'rand')->limit(3)->get();

        return view('home')->with(compact('hotTours'));
    }

    /**
     * Show the application dashboard.
     *
     */
    public function catalog()
    {
        $sort = isset($_GET['sort']) ? $_GET['sort'] : '';

        $tours = $this->getToursWithSort($sort);

        $reviews = Review::orderBy('id', 'rand')->limit(10)->get();

        return view('catalog')->with(compact('tours', 'reviews'));
    }

    public function singleTour(Tour $tour)
    {
        return view('single-tour')->with(compact('tour'));
    }

    public function oNas()
    {
        return view('o-nas');
    }

    public function kontakty()
    {
        return view('kontakty');
    }
}
