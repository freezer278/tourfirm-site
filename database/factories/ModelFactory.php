<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Tour::class, function (Faker\Generator $faker) {


//    $faker = Faker\Generator::create('uk_UA');

    return [
        'hotel_name' => $faker->company,
        'dest_city' => $faker->city,
        'dest_country' => $faker->country,
        'price' => $faker->numberBetween(5000, 50000),
        'stars' => $faker->numberBetween(1, 5),
        'days_in_tour' => $faker->numberBetween(2, 30),
    ];
});

$factory->define(App\TourImage::class, function (Faker\Generator $faker) {

    $faker->addProvider(new EmanueleMinotto\Faker\PlaceholdItProvider($faker));

//    $faker = Faker\Generator::create('uk_UA');

    return [
        'path' => $faker->imageUrl(1920, 'jpeg'),
        'main_image' => 1,
    ];
});

$factory->define(\App\Review::class, function (Faker\Generator $faker) {

//    $faker->addProvider(new EmanueleMinotto\Faker\PlaceholdItProvider($faker));

//    $faker = Faker\Generator::create('uk_UA');

    return [
        'name' => $faker->name,
        'text' => $faker->text(255),
        'created_at' => $faker->dateTimeBetween('-5 years'),
    ];
});
