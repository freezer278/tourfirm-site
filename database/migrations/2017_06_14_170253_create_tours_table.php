<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hotel_name');
            $table->string('dest_city');
            $table->string('dest_country');
            $table->float('price')->default(0);
            $table->tinyInteger('stars')->default(1);
            $table->tinyInteger('days_in_tour')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tours');
        Schema::enableForeignKeyConstraints();
    }
}
