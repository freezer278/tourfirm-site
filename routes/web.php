<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/catalog', 'HomeController@catalog')->name('catalog');
Route::get('/o-nas', 'HomeController@oNas');
Route::get('/kontakty', 'HomeController@kontakty');

Route::get('/test', function () {
//    factory(\App\Tour::class, 50)->create()->each(function ($u) {
//        $u->images()->save(factory(App\TourImage::class)->make());
//    });

    factory(\App\Review::class, 50)->create();
});

Route::get('/{tour}', 'HomeController@singleTour');