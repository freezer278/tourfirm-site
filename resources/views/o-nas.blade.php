@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Про нас</div>

                <div class="panel-body">
          <p style="text-align: justify;">Турагенція «Sun Tour» – одна з найкращих, надійних та перспективних турагенцій України. На туристичному ринку ми існуємо більш ніж 2 роки, нам довірилась велика кількість туристів. Ми пишаємось тим, що зайняли перше місце в номінації найкраще мережеве туристичне агентство. </p>
<p style="text-align: center;"><strong>Sun Tor везе тебе до мрій!</strong></p>
<p style="text-align: justify;">Sun Tour&nbsp; надає нашим клієнтам можливість отримати кращий туристичний продукт за доступними цінами. Наші менеджери регулярно покращують свій професіоналізм, а також оглядають особисто готельну базу, що зводить Ваш час для пошуку відпочинку до мінімума. Ми працюємо для вас в деяких випадках 24 год на добу. Наші менеджери надають підтримку туристам протягом всього терміну відпочинку до їхнього повернення.</p>
<p style="text-align: justify;">На нас можна&nbsp;покластись! Всі <a href="home" target="_blank">види турів</a> в будь-який&nbsp;куточок світу за найкращими цінами на туристичному ринку!</p>
<p style="text-align: justify;"><strong>Для бронювання туру вам необхідно зробити 3 простих кроки:</strong></p>
<p style="text-align: justify;">1) &nbsp;Обрати бажаний тур за допомогою наших менеджерів.<br>
2) &nbsp;Забронювати тур онлайн* або прийти до нас в офіс для підписання договору.<br>
3) Отримати документи на подорож будь-яким зручним для вас способом: завітати до нас в офіс, за допомогою кур’єра або на електронну скриньку.</p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
