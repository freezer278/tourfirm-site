@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $tour->hotel_name }}</div>

                <div class="panel-body">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            @foreach($tour->images as $image)
                                @if($loop->first)
                                    <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}" class="active"></li>
                                @else
                                    <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}"></li>
                                @endif
                            @endforeach
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            @foreach($tour->images as $image)
                                @if($loop->first)
                                    <div class="item active">
                                        <img src="{{ $image->path }}">
                                    </div>
                                @else
                                    <div class="item">
                                        <img src="{{ $image->path }}">
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <h2>Готель: {{ $tour->hotel_name }}</h2>
                    <h3>Місце: {{ $tour->destination }}</h3>
                    <h3>Ціна: {{ $tour->price }}</h3>
                    <h3>Зірки: {{ $tour->stars }}</h3>
                    <h3>Дні в турі: {{ $tour->days_in_tour }}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
