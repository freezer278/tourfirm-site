@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Контакти</div>

                <div class="panel-body">

<p ><strong>Центральний офіс в Києві</strong></p>
                        <br>
                        Адреса:<br>
                        03057, г. Київ, ул. Вадима Гетьмана, 3-А, офіс 27 (1 поверх)&nbsp;<br>
                        <br>                  
<p style="text-align: center;"><strong>Регіональні представництва</strong></p>
                        <br>
<p ><strong>Філіал в Дніпре</strong></p>
                        49000, г.Дніпро, ул.В. Мономаха, 8 (вул. Московська)<br>
                        Телефон: +38 (0562) 314-545<br>
                        Факс: +38 (0562) 314-646<br>
                        E-mail: dnipro@sun.tour<br>
                        <br>
                        Графік работи:&nbsp;<br>
                        Понеділок-п'ятница: с 09:00 до 19:00<br>
                        Субота: с 10:00 до 16:00<br>
                        Неділя: вихідний<br>
                        <br>
<p ><strong>Філіал у Львові</strong></p>
                        <br>
                        Україна, 79000, г. Львів, ул. Костюшка, 8</p>

                    <p>Телефон: +38 (032) 244-55-44</p>

                    <p>Факс: +38 (032) 244-55-45</p>

                    <p>E-mail: <a href="mailto:ekb@lvov@sun.tour">lvіv@sun.tour</a><br>
                        <br>
                        График работы:&nbsp;<br>
                        Понеділок-п'ятница: с 09:00 до 19:00<br>
                        Субота: с 11:00 до 16:00<br>
                        Неділя: вихідний<br>
                        &nbsp;<br>
<p ><strong>Філіал в Харкові</strong></p>
                        </p>

                    <p>Україна, 61057, г. Харків, ул.Чернишевська 13, БЦ "Ліра", оф. 504</p>

                    <p>Телефон: +38 (057) 76-27-666,<br>
                        +38 (057) 76-28-444</p>

                    <p>E-mail: <a href="">kharkшv@sun.tour</a><br>
                        <br>
                        Графік работи:&nbsp;<br>
                        Понеділок-п'ятница: с 09:00 до 19:00<br>
                        Субота: с 11:00 до 16:00<br>
                        Неділя: вихідний<br>
                        &nbsp;<br>
                        Філиал в Одесі<br>
                        65011, г. Одеса, ул. Базарна 80, офіс 101 (вхід з вул. Єкатерининської)<br>
                        Телефон: +38 (048) 729-73-73<br>
                        E-mail: <a href="mailto:odesa@sun.tour">odesa@sun.tour</a><br>
                        <br>
                        Графік работи:&nbsp;<br>
                        Понеділок-п'ятница: с 09:00 до 19:00<br>
                        Субота: з 11:00 до 16:00<br>
                        Неділя: вихідний</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
