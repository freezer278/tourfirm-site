@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <select id="sort" title="сортування" onchange="applySort(this)">
                        <option>Сортування</option>
                        <option value="{{ \App\Tour::SORT_PRICE_ASC }}" @if(isset($_GET['sort']) && $_GET['sort'] === \App\Tour::SORT_PRICE_ASC) selected @endif>Ціна зростання</option>
                        <option value="{{ \App\Tour::SORT_PRICE_DESC }}" @if(isset($_GET['sort']) && $_GET['sort'] === \App\Tour::SORT_PRICE_DESC) selected @endif>Ціна спадання</option>
                        <option value="{{ \App\Tour::SORT_STARS_ASC }}" @if(isset($_GET['sort']) && $_GET['sort'] === \App\Tour::SORT_STARS_ASC) selected @endif>Зірки зростання</option>
                        <option value="{{ \App\Tour::SORT_STARS_DESC }}" @if(isset($_GET['sort']) && $_GET['sort'] === \App\Tour::SORT_STARS_DESC) selected @endif>Зірки спадання</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default col-md-10">
                <div class="panel-heading">Тури</div>

                <div class="panel-body">
                    <div class="col-md-12">
                        <h2>Всі тури</h2>
                        @foreach($tours as $tour)
                            <div class="col-md-6 single-tour">
                                <div class="col-md-5 tour-image-container">
                                    @if($tour->mainImage() != null)
                                        <img src="{{ url($tour->mainImage()->path)}}">
                                    @else
                                        <img src="">
                                    @endif
                                </div>
                                <div class="col-md-7 tour-body">
                                    <h3>Готель: {{ $tour->hotel_name }}</h3>
                                    <h4>Місце: {{ $tour->destination }}</h4>
                                    <h4>Ціна: {{ $tour->price }} грн</h4>
                                    <h4>Зірки: {{ $tour->stars }}</h4>
                                    <h4>Дні в турі: {{ $tour->days_in_tour }}</h4>
                                    <a class="btn btn-default" href="{{ url('/'.$tour->id) }}">
                                        Деталі
                                    </a>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>

                {{ $tours->links() }}
            </div>

            <div class="panel panel-default col-md-2 reviews-panel">
                <div class="panel-heading">Відгуки</div>

                <div class="panel-body">
                    @foreach($reviews as $review)
                        <div class="col-md-12 single-review">
                            <h6>{{ $review->name }}</h6>
                            <p>{{ $review->text }}</p>
                            <span>{{ $review->created_at->format('d.m.Y') }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var url = '{{ route('catalog') }}';

        function applySort(select) {
            var value = $(select).val();

            window.location.href = url + '?sort=' + value;
        }
    </script>
@endsection
