@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Гарячі Тури</div>

                <div class="panel-body">

                    <div class="col-md-10 col-md-offset-1 top-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                    </div>

                    @foreach($hotTours as $tour)
                        <div class="col-md-12 single-tour">
                            <div class="col-md-4 tour-image-container">
                                @if($tour->mainImage() != null)
                                    <img src="{{ url($tour->mainImage()->path)}}">
                                @else
                                    <img src="">
                                @endif
                            </div>
                            <div class="col-md-8 tour-body">
                                <h2>Готель: {{ $tour->hotel_name }}</h2>
                                <h3>Місце: {{ $tour->destination }}</h3>
                                <h3>Ціна: {{ $tour->price }} грн</h3>
                                <h3>Зірки: {{ $tour->stars }}</h3>
                                <h3>Дні в турі: {{ $tour->days_in_tour }}</h3>
                                <a class="btn btn-default" href="{{ url('/'.$tour->id) }}">
                                    Деталі
                                </a>
                            </div>
                        </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
